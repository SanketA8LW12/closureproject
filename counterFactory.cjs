// Return an object that has two methods called `increment` and `decrement`.
// `increment` should increment a counter variable in closure scope and return it.
// `decrement` should decrement the counter variable and return it.

function counterFactory() {
    let counter = 0;

    // returning as a object
    return {  
         increment(){
            return ++counter;
        },
        decrement(){
            return --counter;
        }
    }

}

module.exports = counterFactory;