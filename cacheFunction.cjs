// Should return a function that invokes `cb`.
// A cache (object) should be kept in closure scope.
// The cache should keep track of all arguments have been used to invoke this function.
// If the returned function is invoked with arguments that it has already seen
// then it should return the cached result and not invoke `cb` again.
// `cb` should only ever be invoked once for a given set of arguments.

function cacheFunction(cb) {

    if(typeof cb !== 'function'){
        throw new Error("The provided callBack function is not valid");
    }

    let cacheObject = {};

    return function(...args){

        let value = JSON.stringify(args);
        if(value in cacheObject){
            // if value is present in object return from that 
            //console.log("taking value from obj");
            return cacheObject[value];
        }
        else{

            let result = cb(...args);
            cacheObject[value] = result; // adding value to object
            //console.log("Added value to obj");
            return result;
        }

    }
}

module.exports = cacheFunction;