let cacheFunctionTest =  require('./../cacheFunction.cjs');

function cb(args){
    args += 1;
    return args;
}

try{
    let result = cacheFunctionTest(cb);
    console.log(result(1));
    console.log(result(2));
    console.log(result(2));
    console.log(result(2));
    console.log(result(5));
} 
catch(Error){
    console.log(Error);
}