// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned

function limitFunctionCallCount(cb, n) {

    if(typeof cb !== 'function' || typeof n !== 'number'){
        throw new Error("The provided prameters are not valid");
    }

    let count = 0;

    return function(...args){
        if(count < n){
            count++;
            return cb(...args);
        }
        else{
            return null;
        }
    }

}

module.exports = limitFunctionCallCount;

// article hwo to throw the error in JavaScript
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/throw